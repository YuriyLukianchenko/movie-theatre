Deafault Events:

event name | event price | event rating |  auditorium | airTime        |
-----------|-------------|--------------|-------------|----------------|
KZK        | 55          | medium       | BlueHall    | 2020-6-1 15:00 |
KZK        | 55          | medium       | BlueHall    | 2020-6-2 18:00 |
KZK        | 55          | medium       | BlueHall    | 2020-6-4 10:00 |
StarCat    | 90          | high         | RedHall     | 2020-6-1 23:00 |
StarCat    | 90          | high         | RedHall     | 2020-6-3 16:20 |
StarCat    | 90          | high         | BlueHall    | 2020-6-5 11:30 |
VanKycios  | 10.50       | low          | GreenHall   | 2020-5-31 9:40 |
VanKycios  | 10.50       | low          | GreenHall   | 2020-6-1 14:30 |
VanKycios  | 10.50       | low          | RedHall     | 2020-5-2 23:30 |
------------------------------------------------------------------------