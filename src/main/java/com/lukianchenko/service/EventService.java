package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Rating;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Service, is used to : manage events.
 */
public interface EventService {

    /**
     * Create event.
     *
     * @param name      of the event
     * @param basePrice of the event.
     * @param rating    of the event.
     */
    void createEvent(String name, double basePrice, Rating rating);

    /**
     * Get event by name.
     *
     * @param name of the event.
     * @return event.
     */
    Event getByName(String name);

    /**
     * Remove event.
     *
     * @param name of the event.
     */
    void remove(String name);

    /**
     * Get all events.
     *
     * @return set of the events
     */
    Collection<Event> getAll();

    /**
     * Clear repository, remove all events.
     */
    void clearRepository();

    /**
     * Update event rating.
     *
     * @param name   of the event.
     * @param rating of the event.
     */
    void updateEventRating(String name, Rating rating);

    /**
     * Update event base price.
     *
     * @param name      of the event.
     * @param basePrice of the event.
     */
    void updateEventBasePrice(String name, double basePrice);

    /**
     * Update airdates of the event
     *
     * @param name       of the event.
     * @param time       when event will be shown.
     * @param auditorium were event will be shown.
     */
    void updateEventAirDates(String name, LocalDateTime time, Auditorium auditorium);
}
