package com.lukianchenko.service;

import com.lukianchenko.domain.Ticket;
import com.lukianchenko.domain.User;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Service which manages users.
 */
public interface UserService {

    /**
     * Create user.
     *
     * @param email of the user.
     * @param admin shows is user has admin role.
     * @param birthday of user.
     */
    void createUser(String email, boolean admin, LocalDateTime birthday);

    /**
     * Get user by email.
     *
     * @param email of the user.
     * @return user with specific email.
     */
    User getByEmail(String email);

    /**
     * Get all users.
     *
     * @return collection of users.
     */
    Collection<User> getAll();

    /**
     * Remove user with specific email.
     *
     * @param email of the user.
     */
    void remove(String email);

    /**
     * Update user role.
     *
     * @param email     of the user.
     * @param adminRole can be true or false.
     */
    void updateUserRole(String email, boolean adminRole);

    /**
     * Add booked tickets to user history of booked tickets.
     *
     * @param email  of the user.
     * @param ticket need to be added to the user history.
     */
    void addUserTicket(String email, Ticket ticket);


}
