package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;

import java.util.Set;

/**
 * Service which gives methods to manage auditoriums.
 */
public interface AuditoriumService {

    /**
     * Get all auditoriums.
     *
     * @return all auditoriums.
     */
    Set<Auditorium> getAll();

    /**
     * Get auditorium bt name.
     *
     * @param name of the auditorium ("BlueHall, RedHall, GreenHall")
     * @return auditorium.
     */
    Auditorium getByName(String name);
}
