package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.repository.AuditoriumRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    private static final Logger log = LogManager.getLogger(AuditoriumServiceImpl.class);
    private AuditoriumRepository auditoriumRepository;

    @Autowired
    public AuditoriumServiceImpl(
            @Qualifier("auditoriumDerbyRepository")
            AuditoriumRepository auditoriumRepository) {
        this.auditoriumRepository = auditoriumRepository;
    }

    public Set<Auditorium> getAll() {
        return auditoriumRepository.getAll();
    }

    public Auditorium getByName(String name) {
        Auditorium auditorium = auditoriumRepository.getByName(name);
        if (auditorium == null) {
            log.info(String.format("Auditorium with name '%s' does not exist", name));
        }
        return auditorium;
    }
}
