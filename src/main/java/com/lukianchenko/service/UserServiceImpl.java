package com.lukianchenko.service;

import com.lukianchenko.domain.Ticket;
import com.lukianchenko.domain.User;
import com.lukianchenko.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger log =
            LogManager.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(
            @Qualifier("userDerbyRepository")
            UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(String email, boolean admin, LocalDateTime birthday) {

        if (!isEmailExists(email)) {
            User user = new User(email, admin, birthday , new HashSet<>());
            userRepository.save(email, user);
            log.info(String.format("User '%s' was registered", email));
        } else {
            log.info(String.format("User '%s' already exists", email));
        }
    }

    @Override
    public User getByEmail(String email) {
        User user = userRepository.getByEmail(email);
        if (user == null) {
            log.info(String.format("No such user '%s' in base", email));
        }
        return user;
    }

    @Override
    public void remove(String email) {
        Optional.ofNullable(userRepository.remove(email))
                .ifPresentOrElse(user -> log.info(String.format("User '%s' was removed", email)),
                        () -> log.info(String.format("Failed to remove, no such user '%s' in base", email)));
    }

    @Override
    public Collection<User> getAll() {
        Collection collection = userRepository.getAll();
        if (collection.isEmpty()) {
            log.info("User database is empty");
        }
        return collection;
    }

    @Override
    public void updateUserRole(String email, boolean adminRole) {
        Optional.ofNullable(userRepository.getByEmail(email))
                .ifPresentOrElse(user -> {
                            user.setAdmin(adminRole);
                            log.info(String.format("Admin role = '%s', was assigned to user '%s'", adminRole, email));
                        },
                        () -> log.info(String.format("No such user '%s' id base", email))
                );
    }

    @Override
    public void addUserTicket(String email, Ticket ticket) {
        Optional.ofNullable(userRepository.getByEmail(email))
                .ifPresentOrElse(user -> {
                            user.getTickets().add(ticket);
                            log.info(String.format("Ticket '%s' was added to '%s' ticket collection", ticket.getId(), email));
                        },
                        () -> log.info(String.format("No such user '%s' id base", email))
                );
    }

    private boolean isEmailExists(String email) {
        return userRepository.getAll().stream()
                .anyMatch(user -> user.getEmail().equals(email));
    }
}

