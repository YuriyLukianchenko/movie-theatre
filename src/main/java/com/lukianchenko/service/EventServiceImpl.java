package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Rating;
import com.lukianchenko.repository.EventRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    private static final Logger log = LogManager.getLogger(EventServiceImpl.class);

    private EventRepository eventRepository;
    private AuditoriumService auditoriumService;

    @Autowired
    public EventServiceImpl(
            @Qualifier("eventDerbyRepository")
            EventRepository eventRepository,
            AuditoriumService auditoriumService) {
        this.eventRepository = eventRepository;
        this.auditoriumService = auditoriumService;
    }

    @PostConstruct
    private void init() {
        createEvent("KZK", 55, Rating.MEDIUM);
        updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 1, 15, 0),
                auditoriumService.getByName("BlueHall"));
        updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 2, 18, 0),
                auditoriumService.getByName("BlueHall"));
        updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 4, 10, 0),
                auditoriumService.getByName("BlueHall"));

        createEvent("StarCat", 90, Rating.HIGH);
        updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 1, 23, 0),
                auditoriumService.getByName("RedHall"));
        updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 3, 16, 20),
                auditoriumService.getByName("RedHall"));
        updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 5, 11, 30),
                auditoriumService.getByName("BlueHall"));

        createEvent("VanKycios", 10.50, Rating.LOW);
        updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 5, 31, 9, 40),
                auditoriumService.getByName("GreenHall"));
        updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 6, 1, 14, 30),
                auditoriumService.getByName("GreenHall"));
        updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 6, 2, 23, 30),
                auditoriumService.getByName("RedHall"));
    }
    @Override
    public void createEvent(String name, double basePrice, Rating rating) {

        if (!isNameExists(name)) {
            Event event = new Event(name, basePrice, rating);
            eventRepository.save(name, event);
            log.info(String.format("event '%s' was saved", name));
        } else {
            log.info(String.format("event '%s' already exists", name));
        }
    }

    @Override
    public Event getByName(String name) {
        Event event = eventRepository.getByName(name);
        if (event == null) {
            log.info(String.format("No such event '%s' in base", name));
        }
        return event;
    }

    @Override
    public void remove(String name) {
        Optional.ofNullable(eventRepository.remove(name))
                .ifPresentOrElse(event -> log.info(String.format("event '%s' was removed", name)),
                        () -> log.info(String.format("Failed to remove, no such event '%s' in base", name)));
    }

    @Override
    public Collection<Event> getAll() {
        Collection collection = eventRepository.getAll();
        if (collection.isEmpty()) {
            log.info("event database is empty");
        }
        return collection;
    }

    @Override
    public void clearRepository() {
        eventRepository.clear();
    }

    @Override
    public void updateEventRating(String name, Rating rating) {
        Optional.ofNullable(eventRepository.getByName(name))
                .ifPresentOrElse(event -> {
                            event.setRating(rating);
                            eventRepository.remove(event.getName());
                            eventRepository.save(event.getName(),event);
                            log.info(String.format("New rating = '%s', was assigned to event '%s'", rating, name));
                        },
                        () -> log.info(String.format("No such event '%s' in base", name))
                );
    }
    @Override
    public void updateEventBasePrice(String name, double basePrice) {
        Optional.ofNullable(eventRepository.getByName(name))
                .ifPresentOrElse(event -> {
                            event.setBasePrice(basePrice);
                            eventRepository.remove(event.getName());
                            eventRepository.save(event.getName(),event);
                            log.info(String.format("New base price '%s', was assigned to event '%s'", basePrice, name));
                        },
                        () -> log.info(String.format("No such event '%s' id base", name))
                );
    }

    @Override
    public void updateEventAirDates(String name, LocalDateTime time, Auditorium auditorium) {
        Optional.ofNullable(eventRepository.getByName(name))
                .ifPresentOrElse(event -> {
                            event.getAirDates().put(time, auditorium);
                            eventRepository.remove(event.getName());
                            eventRepository.save(event.getName(),event);
                            log.info(String.format("Air date '%s', was assigned to event '%s'", time, name));
                        },
                        () -> log.info(String.format("No such event '%s' id base", name))
                );
    }
    

    private boolean isNameExists(String name) {
        return eventRepository.getAll().stream()
                .anyMatch(event -> event.getName().equals(name));
    }
}
