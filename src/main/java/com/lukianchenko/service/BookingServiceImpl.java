package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Ticket;
import com.lukianchenko.domain.User;
import com.lukianchenko.repository.BookingRepository;
import com.lukianchenko.service.discount.DiscountService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {

    private BookingRepository bookingRepository;
    private EventService eventService;
    private UserService userService;
    private DiscountService discountService;

    private static final Logger log = LogManager.getLogger(BookingServiceImpl.class);

    @Autowired
    public BookingServiceImpl(

            @Qualifier("bookingDerbyRepository")
            BookingRepository bookingRepository,
            EventService eventService,
            UserService userService,
            DiscountService discountService) {
        this.bookingRepository = bookingRepository;
        this.eventService = eventService;
        this.userService = userService;
        this.discountService = discountService;
    }

    @Override
    public Set<Ticket> createTickets(String eventName, LocalDateTime airTime, String userEmail,
                                     Set<Integer> seats) {

        Event currentEvent = eventService.getByName(eventName);
        if (currentEvent == null) {
            log.info(String.format("No such event %s", eventName));
            return null;
        }

        LocalDateTime currentAirTime = currentEvent.getAirDates().keySet().stream()
                .filter(time -> time.toString().equals(airTime.toString()))
                .findAny()
                .orElse(null);
        if (currentAirTime == null) {
            log.info(String.format("No such airTime %s", airTime));
            return null;
        }

        Auditorium currentAuditorium = currentEvent.getAirDates().get(currentAirTime);

        seats.stream()
                .filter(seatNumber -> seatNumber > currentAuditorium.getNumberOfSeats())
                .forEach(seat -> log.info(String.format("Seat number %s does not exist", seat)));

        if (seats.stream()
                .anyMatch(seatNumber -> seatNumber > currentAuditorium.getNumberOfSeats())) {
            return null;
        }

        return seats.stream()
                .map(seat -> new Ticket(userEmail, currentEvent, currentAuditorium, currentAirTime, seat))
                .collect(Collectors.toSet());
    }

    @Override
    public double getPrice(String eventName, LocalDateTime airTime, String userEmail,
                           Set<Integer> seats, LocalDateTime birthDay) {

        Set<Ticket> tickets = createTickets(eventName, airTime, userEmail, seats);
        if (tickets == null) {
            return -1;
        }
        double price = tickets.stream()
                .mapToDouble(this::calculateTicketPrice)
                .sum();
        int userTicketsNumber;
        User user = userService.getByEmail(userEmail);
        if (user != null) {
            if (user.getTickets().size() != 0) {
                userTicketsNumber = user.getTickets().size() + tickets.size();
            } else {
                userTicketsNumber = tickets.size();
            }

        } else {
            userTicketsNumber = tickets.size();
        }

        double discount = discountService.getDiscount(userTicketsNumber, airTime, birthDay);

        return price * (1 - discount / 100);
    }

    @Override
    public void bookTickets(Set<Ticket> tickets) {
        tickets.forEach(ticket -> {
                    bookingRepository.save(ticket.getId().toString(), ticket);
                    log.info(String.format("Ticket number %s was booked", ticket.getId().toString()));
                }
        );
    }

    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
        return (Set) bookingRepository.getTicketsByEventAndDateTime(event.getName(), dateTime);
    }

    @Override
    public Set<Ticket> getAllPurchasedTickets() {
        return new HashSet<>(bookingRepository.getAll());
    }

    private double calculateTicketPrice(Ticket ticket) {
        Double basePrice = ticket.getEvent().getBasePrice();
        double ratingFactor;
        double vipFactor;
        switch (ticket.getEvent().getRating()) {
            case LOW:
                ratingFactor = 0.8;
                break;
            case MEDIUM:
                ratingFactor = 1;
                break;
            case HIGH:
                ratingFactor = 1.2;
                break;
            default:
                ratingFactor = 1;
        }
        if (isSeatVIP(ticket.getSeatNumber(), ticket.getAuditorium())) {
            vipFactor = 2;
        } else {
            vipFactor = 1;
        }
        return basePrice * vipFactor * ratingFactor;
    }

    private boolean isSeatVIP(int seatNumber, Auditorium auditorium) {
        return auditorium.getVipSeats().stream()
                .anyMatch(seat -> seat.equals(new Long(seatNumber)));
    }
}
