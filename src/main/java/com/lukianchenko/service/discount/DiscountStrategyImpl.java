package com.lukianchenko.service.discount;

import com.lukianchenko.repository.AuditoriumRepositoryWithStaticCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public class DiscountStrategyImpl {

    private static final Logger log =
            LogManager.getLogger(DiscountStrategyImpl.class);
    protected Properties discountProperties = new Properties();
    private InputStream STREAM_TO_PROPERTIES_FILE = getClass().getClassLoader().getResourceAsStream("application.properties");

    {
        try {
            discountProperties.load(STREAM_TO_PROPERTIES_FILE);
        } catch (IOException e) {
            log.info("Failed to load properties file");
        }
    }

}
