package com.lukianchenko.service.discount;

import java.time.LocalDateTime;

/**
 * Service is used to obtain discount using all strategies.
 */
@FunctionalInterface
public interface DiscountService {

    /**
     * Get tickets discount.
     *
     * @param numberOfTickets of specific user, all booked ticket before plus current booked tickets.
     * @param airTime         of the specific event.
     * @param birthday        of the user, who want to get discount.
     * @return final discount after all strategies were applied.
     */
    double getDiscount(int numberOfTickets, LocalDateTime airTime, LocalDateTime birthday);
}
