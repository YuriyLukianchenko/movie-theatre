package com.lukianchenko.service.discount;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AccumulatedStrategy extends DiscountStrategyImpl implements DiscountStrategy {

    @Override
    public double calculateDiscount(int ticketsNumber, LocalDateTime airTime, LocalDateTime birthday) {
        return Math.log(ticketsNumber);
    }
}
