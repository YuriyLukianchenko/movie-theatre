package com.lukianchenko.service.discount;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MorningStrategy extends DiscountStrategyImpl implements DiscountStrategy {

    private double morningTime = Double.parseDouble(discountProperties.getProperty("discount.morningStrategy.morningTime"));
    private double morningDiscount = Double.parseDouble(discountProperties.getProperty("discount.morningStrategy.morningDiscount"));

    private double zeroMorningDiscount = 0;

    @Override
    public double calculateDiscount(int ticketsNumber, LocalDateTime airTime, LocalDateTime birthday) {
        return airTime.getHour() < morningTime ? morningDiscount : zeroMorningDiscount;
    }
}
