package com.lukianchenko.service.discount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;

@Service
public class
DiscountServiceImpl implements DiscountService {

    private Set<DiscountStrategy> discountStrategies;

    @Autowired
    public DiscountServiceImpl(Set<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }

    public DiscountServiceImpl() {
    }

    @Override
    public double getDiscount(int numberOfTickets, LocalDateTime airTime, LocalDateTime birthday) {
        return discountStrategies.stream()
                .mapToDouble(strategy -> strategy.calculateDiscount(numberOfTickets, airTime, birthday))
                .max()
                .orElse(0);
    }

    public Set<DiscountStrategy> getDiscountStrategies() {
        return discountStrategies;
    }

    public void setDiscountStrategies(Set<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }
}
