package com.lukianchenko.service.discount;

import java.time.LocalDateTime;

/**
 * Specific strategy, can be used to calculate discount.
 */
@FunctionalInterface
public interface DiscountStrategy {

    /**
     * Calculate discount depends on strategy.
     *
     * @param ticketsNumber total tickets user booked.
     * @param airTime       of the event.
     * @param birthday      of the user.
     * @return calculated discount by some strategy.
     */
    double calculateDiscount(int ticketsNumber, LocalDateTime airTime, LocalDateTime birthday);
}
