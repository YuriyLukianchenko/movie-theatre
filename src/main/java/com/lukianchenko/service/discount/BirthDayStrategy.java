package com.lukianchenko.service.discount;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class BirthDayStrategy extends DiscountStrategyImpl implements DiscountStrategy {
    private double daysWithinBirthday =
            Double.parseDouble(discountProperties.getProperty("discount.birthdayStrategy.daysWithinBirthday"));
    private double birthdayDiscount =
            Double.parseDouble(discountProperties.getProperty("discount.birthdayStrategy.birthdayDiscount"));
    private double zeroBirthdayDiscount = 0;

    @Override
    public double calculateDiscount(int ticketsNumber, LocalDateTime airTime, LocalDateTime birthday) {
        return Math.abs(airTime.getDayOfYear() - birthday.getDayOfYear()) <= daysWithinBirthday
                ? birthdayDiscount
                : zeroBirthdayDiscount;
    }
}
