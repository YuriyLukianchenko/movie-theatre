package com.lukianchenko.service;

import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Ticket;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Is used to: book, create, check tickets.
 * manage tickets,
 * calculate price of specific tickets and buy them.
 */
public interface BookingService {

    /**
     * Check if ticket with such parameters can be created.
     *
     * @param eventName .
     * @param airTime   of the event.
     * @param userEmail who book ticket.
     * @param seats     .
     * @return created set of tickets if it is possible.
     */
    Set<Ticket> createTickets(String eventName, LocalDateTime airTime, String userEmail,
                              Set<Integer> seats);

    /**
     * Get price for tickets with specific parameters, also calculate discount which include in price.
     *
     * @param eventName .
     * @param airTime   of the event.
     * @param userEmail who want to book ticket.
     * @param seats .
     * @param birthDay  of the user.
     * @return total price with included discount.
     */
    double getPrice(String eventName, LocalDateTime airTime, String userEmail,
                    Set<Integer> seats, LocalDateTime birthDay);

    /**
     * Book tickets.
     *
     * @param tickets need to be booked.
     */
    void bookTickets(Set<Ticket> tickets);

    /**
     * Get all purchased tickets for specific event and time.
     *
     * @param event    .
     * @param dateTime of the event.
     * @return set of tickets.
     */
    Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime);

    /**
     * Get all purchased tickets.
     *
     * @return set of tickets.
     */
    Set<Ticket> getAllPurchasedTickets();
}
