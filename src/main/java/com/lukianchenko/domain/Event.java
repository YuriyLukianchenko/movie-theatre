package com.lukianchenko.domain;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class  Event {

    private String name;

    private double basePrice;

    private Rating rating;

    private Map<LocalDateTime, Auditorium> airDates = new HashMap<>();

    public Event(String name, double basePrice, Rating rating) {
        this.name = name;
        this.basePrice = basePrice;
        this.rating = rating;
    }

    public Event() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public Map<LocalDateTime, Auditorium> getAirDates() {
        return airDates;
    }

    public void setAirDates(Map<LocalDateTime, Auditorium> airDates) {
        this.airDates = airDates;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", basePrice=" + basePrice +
                ", rating=" + rating +
                ", airDates=" + airDates +
                '}';
    }
}
