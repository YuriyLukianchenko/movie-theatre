package com.lukianchenko.domain;

import java.time.LocalDateTime;
import java.util.Set;

public class User {

    private String email;

    private boolean admin;

    private LocalDateTime birthday;

    private Set<Ticket> tickets;

    public User(String email, boolean admin, LocalDateTime birthday, Set<Ticket> tickets) {
        this.email = email;
        this.admin = admin;
        this.birthday = birthday;
        this.tickets = tickets;
    }

    public User() {
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", admin=" + admin +
                ", tickets=" + tickets +
                '}';
    }
}
