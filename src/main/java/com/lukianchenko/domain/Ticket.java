package com.lukianchenko.domain;

import java.time.LocalDateTime;
import java.util.UUID;

public class Ticket {

    private UUID id;

    private String userEmail;

    private Event event;

    private Auditorium auditorium;

    private LocalDateTime time;

    private int seatNumber;

    public Ticket(String userEmail, Event event, Auditorium auditorium, LocalDateTime time, int seatNumber) {
        this.id = UUID.randomUUID();
        this.userEmail = userEmail;
        this.event = event;
        this.auditorium = auditorium;
        this.time = time;
        this.seatNumber = seatNumber;
    }

    public Ticket() {
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail (String userEmail) {
        this.userEmail = userEmail;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Auditorium getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", userEmail=" + userEmail +
                ", event=" + event +
                ", auditorium=" + auditorium +
                ", time=" + time +
                ", seatNumber=" + seatNumber +
                '}';
    }
}
