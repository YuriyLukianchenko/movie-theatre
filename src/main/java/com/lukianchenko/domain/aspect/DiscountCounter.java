package com.lukianchenko.domain.aspect;

public class DiscountCounter {

    private int morningDiscountCounter;

    private int accumulatedDiscountCounter;

    private int birthdayDiscountCounter;

    public int getMorningDiscountCounter() {
        return morningDiscountCounter;
    }

    public void setMorningDiscountCounter(int morningDiscountCounter) {
        this.morningDiscountCounter = morningDiscountCounter;
    }

    public int getAccumulatedDiscountCounter() {
        return accumulatedDiscountCounter;
    }

    public void setAccumulatedDiscountCounter(int accumulatedDiscountCounter) {
        this.accumulatedDiscountCounter = accumulatedDiscountCounter;
    }

    public int getBirthdayDiscountCounter() {
        return birthdayDiscountCounter;
    }

    public void setBirthdayDiscountCounter(int birthdayDiscountCounter) {
        this.birthdayDiscountCounter = birthdayDiscountCounter;
    }
}
