package com.lukianchenko.domain.aspect;

public class EventCounterInformation {

    private int accessedByNameNumber;
    private int priceQueriedNumber;
    private int bookedTicketNumber;

    public int getAccessedByNameNumber() {
        return accessedByNameNumber;
    }

    public void setAccessedByNameNumber(int accessedByNameNumber) {
        this.accessedByNameNumber = accessedByNameNumber;
    }

    public int getPriceQueriedNumber() {
        return priceQueriedNumber;
    }

    public void setPriceQueriedNumber(int priceQueriedNumber) {
        this.priceQueriedNumber = priceQueriedNumber;
    }

    public int getBookedTicketNumber() {
        return bookedTicketNumber;
    }

    public void setBookedTicketNumber(int bookedTicketNumber) {
        this.bookedTicketNumber = bookedTicketNumber;
    }
}
