package com.lukianchenko.domain;

public enum Rating {
    HIGH,
    MEDIUM,
    LOW
}
