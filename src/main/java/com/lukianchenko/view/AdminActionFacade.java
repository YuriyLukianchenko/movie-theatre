package com.lukianchenko.view;

import com.lukianchenko.domain.Rating;
import com.lukianchenko.domain.Ticket;
import com.lukianchenko.service.BookingService;
import com.lukianchenko.service.EventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.Set;

@Component
public class AdminActionFacade {

    private static final Logger log =
            LogManager.getLogger(AdminActionFacade.class);

    private EventService eventService;
    private BookingService bookingService;

    @Autowired
    public AdminActionFacade(EventService eventService, BookingService bookingService) {
        this.eventService = eventService;
        this.bookingService = bookingService;
    }

    public void enterEvent() {
        boolean inputRatingFlag = true;
        Scanner scanner = new Scanner(System.in);
        log.info("input event name");
        String eventName = scanner.nextLine();
        log.info("input event base price");
        double basePrice = Double.parseDouble(scanner.nextLine());
        Rating rating = null;
        while (inputRatingFlag) {
            log.info("input rating (high, medium, low)");
            String ratingString = scanner.nextLine();
            switch (ratingString) {
                case "high":
                    rating = Rating.HIGH;
                    inputRatingFlag = false;
                    break;
                case "medium":
                    inputRatingFlag = false;
                    rating = Rating.MEDIUM;
                    break;
                case "low":
                    rating = Rating.LOW;
                    inputRatingFlag = false;
                    break;
                default:
                    log.info("unknown rating");
            }
        }
        eventService.createEvent(eventName, basePrice, rating);
    }

    public void viewPurchasedTickets() {
        Set<Ticket> tickets = bookingService.getAllPurchasedTickets();
        if(tickets.size() == 0) {
            log.info("There are no purchased tickets");
        } else {
        tickets.forEach(log::info);
        }
    }
}
