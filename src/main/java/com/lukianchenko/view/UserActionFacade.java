package com.lukianchenko.view;

import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Ticket;
import com.lukianchenko.service.BookingService;
import com.lukianchenko.service.EventService;
import com.lukianchenko.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class UserActionFacade {

    private static final Logger log =
            LogManager.getLogger(UserActionFacade.class);

    private UserService userService;
    private EventService eventService;
    private BookingService bookingService;

    @Autowired
    public UserActionFacade(UserService userService, EventService eventService, BookingService bookingService) {
        this.userService = userService;
        this.eventService = eventService;
        this.bookingService = bookingService;
    }

    public void registerUser() {
        Scanner scanner = new Scanner(System.in);
        log.info("please input user email");
        String email = scanner.nextLine();
        log.info("please input user isAdmin: 'true' or 'false'");
        boolean isAdmin = Boolean.parseBoolean(scanner.nextLine());
        log.info("please input birthday yyyy-mm-dd-hh-mm (integer values)");
        log.info("please input year");
        int yearb = Integer.parseInt(scanner.nextLine());
        log.info("please input month");
        int monthb = Integer.parseInt(scanner.nextLine());
        log.info("please input day");
        int dayb = Integer.parseInt(scanner.nextLine());
        log.info("please input hh (0-24)");
        int hourb = Integer.parseInt(scanner.nextLine());
        log.info("please input minute");
        int minuteb = Integer.parseInt(scanner.nextLine());
        LocalDateTime birthday = LocalDateTime.of(yearb, monthb, dayb, hourb, minuteb);
        userService.createUser(email, isAdmin, birthday);
    }

    public void viewEvents() {
        Scanner scanner = new Scanner(System.in);
        log.info("please input airtime yyyy-mm-dd-hh-mm (integer values)");
        log.info("please input year");
        int year = Integer.parseInt(scanner.nextLine());
        log.info("please input month");
        int month = Integer.parseInt(scanner.nextLine());
        log.info("please input day");
        int day = Integer.parseInt(scanner.nextLine());
        log.info("please input hh (0-24)");
        int hour = Integer.parseInt(scanner.nextLine());
        log.info("please input minute");
        int minute = Integer.parseInt(scanner.nextLine());
        LocalDateTime airDate = LocalDateTime.of(year, month, day, hour, minute);
        Set<Event> events = eventService.getAll().stream()
                .filter(event -> event.getAirDates().containsKey(airDate))
                .collect(Collectors.toSet());

        if (events.size() == 0) {
            log.info("No events on this time");
        } else {
            events.forEach(event -> log.info(event.getName()));
        }
    }

    public void viewAllEvents() {
        eventService.getAll().forEach(log::info);
    }

    public void getTicketsPrice() {
        Scanner scanner = new Scanner(System.in);
        log.info("please input data to get tickets price");
        log.info("please input eventName");
        String eventName = scanner.nextLine();
        log.info("please input airtime yyyy-mm-dd-hh-mm (integer values)");
        log.info("please input year");
        int year = Integer.parseInt(scanner.nextLine());
        log.info("please input month");
        int month = Integer.parseInt(scanner.nextLine());
        log.info("please input day");
        int day = Integer.parseInt(scanner.nextLine());
        log.info("please input hh (0-24)");
        int hour = Integer.parseInt(scanner.nextLine());
        log.info("please input minute");
        int minute = Integer.parseInt(scanner.nextLine());
        LocalDateTime airTime = LocalDateTime.of(year, month, day, hour, minute);
        log.info("please input user email");
        String userEmail = scanner.nextLine();
        log.info("please input seats (example: 2, 3, 4, 5)");
        String[] seatsStringArray = scanner.nextLine().split(",");
        Set<Integer> seats = Stream.of(seatsStringArray)
                .map(StringUtils::trimAllWhitespace)
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
        log.info("please input birthday yyyy-mm-dd-hh-mm (integer values)");
        log.info("please input year");
        int yearb = Integer.parseInt(scanner.nextLine());
        log.info("please input month");
        int monthb = Integer.parseInt(scanner.nextLine());
        log.info("please input day");
        int dayb = Integer.parseInt(scanner.nextLine());
        log.info("please input hh (0-24)");
        int hourb = Integer.parseInt(scanner.nextLine());
        log.info("please input minute");
        int minuteb = Integer.parseInt(scanner.nextLine());
        LocalDateTime birthday = LocalDateTime.of(yearb, monthb, dayb, hourb, minuteb);


        double price = bookingService.getPrice(eventName, airTime, userEmail, seats, birthday);
        if (price == -1) {
            log.info("Can not calculate price for such parameters");
            return;
        }
        log.info(String.format("Price = %s", price));
        boolean buyFlag = true;
        while (buyFlag) {
            log.info("do yo want buy? (input 'yes' or 'no')");
            String answer = scanner.nextLine();
            if (answer.equals("yes")) {
                Set<Ticket> preparedTickets = bookingService.createTickets(eventName, airTime, userEmail, seats);
                bookingService.bookTickets(preparedTickets);
                buyFlag = false;
            } else if (answer.equals("no")) {
                buyFlag = false;
            }
        }
    }

    public Set<Ticket> prepareTickets(String eventName, LocalDateTime airTime, String userEmail,
                                      Set<Integer> seats) {
        return bookingService.createTickets(eventName, airTime, userEmail, seats);
    }

    public void buyTickets(Set<Ticket> tickets) {
        bookingService.bookTickets(tickets);
    }
}
