package com.lukianchenko.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Scanner;

@Component
@Resource(name = "consoleView")
public class ConsoleView {

    private UserActionFacade userActionFacade;
    private AdminActionFacade adminActionFacade;

    @Autowired
    public ConsoleView(UserActionFacade userActionFacade, AdminActionFacade adminActionFacade) {
        this.userActionFacade = userActionFacade;
        this.adminActionFacade = adminActionFacade;
    }

    private static final Logger log =
            LogManager.getLogger(ConsoleView.class);
    Scanner scanner = new Scanner(System.in);
    String mainRole;

    public void run() {
        showRoleChooser();
        chooseRole();
        if (mainRole.equals("user")) {
            runUserFacade();
        } else {
            runAdminFacade();
        }

    }

    private void chooseRole() {
        boolean flag = true;
        while (flag) {
            String role = scanner.nextLine();
            if (role.equals("admin")) {
                log.info("Admin role actions:");
                mainRole = "admin";
                flag = false;
            } else if (role.equals("user")) {
                log.info("User role actions:");
                mainRole = "user";
                flag = false;
            } else {
                log.info("Unknown role, please enter 'admin' or 'user' role");
            }
        }
    }

    private void runUserFacade() {
        boolean flag = true;
        Scanner userScanner = new Scanner(System.in);
        while (flag) {
            showUserActions();
            switch (userScanner.nextLine()) {
                case "1":
                    userActionFacade.registerUser();
                    break;
                case "2":
                    userActionFacade.viewEvents();
                    break;
                case "3":
                    userActionFacade.viewAllEvents();
                    break;
                case "4":
                    userActionFacade.getTicketsPrice();
                    break;
                case "5":
                    run();
                    break;
                case "6":
                    flag = false;
                    break;
                default:
                    log.info("Unknown operation");
                    break;
            }
        }
    }

    private void runAdminFacade() {
        boolean flag = true;
        Scanner adminScanner = new Scanner(System.in);
        while (flag) {
            showAdminActions();
            switch (adminScanner.nextLine()) {
                case "1":
                    adminActionFacade.enterEvent();
                    break;
                case "2":
                    adminActionFacade.viewPurchasedTickets();
                    break;
                case "3":
                    run();
                    break;
                case "4":
                    flag = false;
                    break;
                default:
                    log.info("Unknown operation");
                    break;
            }
        }
    }

    private void showUserActions() {
        log.info("input '1' - register user");
        log.info("input '2' - view events by date");
        log.info("input '3' - view all events");
        log.info("input '4' - get ticket price and buy tickets");
        log.info("input '5' - choose another role");
        log.info("input '6' - close program");


    }

    private void showAdminActions() {
        log.info("input '1' - enter new event");
        log.info("input '2' - view purchased tickets");
        log.info("input '3' - choose another role");
        log.info("input '4' - close program");

    }

    private void showRoleChooser() {
        log.info("input role, admin or user");
    }

}
