package com.lukianchenko.repository;

import com.lukianchenko.domain.Event;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EventRepositoryWithStaticCollection implements EventRepository {

    private static Map<String, Event> events = new HashMap<>();

    @Override
    public void save(String name, Event event) {
        events.put(name, event);
    }

    @Override
    public Event getByName(String name) {
        return events.values().stream()
                .filter(event -> event.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Event remove(String name) {
        return events.remove(name);
    }


    @Override
    public Collection<Event> getAll() {
        return events.values();
    }

    @Override
    public void clear() {
        events.clear();
    }
}
