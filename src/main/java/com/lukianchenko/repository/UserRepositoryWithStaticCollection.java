package com.lukianchenko.repository;

import com.lukianchenko.domain.User;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserRepositoryWithStaticCollection implements UserRepository {

    private static Map<String, User> users = new HashMap<>();

    @Override
    public void save(String email, User user) {
        users.put(email, user);
    }

    @Override
    public User getByEmail(String email) {
        return users.values().stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User remove(String email) {
        return users.remove(email);
    }


    @Override
    public Collection<User> getAll() {
        return users.values();
    }
}
