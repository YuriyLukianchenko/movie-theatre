package com.lukianchenko.repository;

import com.lukianchenko.domain.Ticket;
import com.lukianchenko.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserDerbyRepository implements UserRepository {

    private static final Logger log = LogManager.getLogger(UserDerbyRepository.class);

    private JdbcTemplate jdbcTemplate;
    private BookingRepository bookingRepository;

    @Autowired
    public UserDerbyRepository(JdbcTemplate jdbcTemplate,
                               @Qualifier("bookingDerbyRepository")
                                       BookingRepository bookingRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.bookingRepository = bookingRepository;
    }


    @Override
    public void save(String email, User user) {
        jdbcTemplate.update("INSERT INTO theatre_user VALUES(?, ?, ?)",
                email,
                Boolean.toString(user.isAdmin()),
                Timestamp.valueOf(user.getBirthday()));
    }

    @Override
    public User getByEmail(String email) {
        try {
            User user = jdbcTemplate.queryForObject("SELECT * FROM theatre_user WHERE email = ?",
                    new Object[]{email},
                    (rs, rowNumber) -> {
                        User innerUser = new User();
                        innerUser.setEmail(email);
                        innerUser.setAdmin(rs.getBoolean("admin"));
                        innerUser.setBirthday(rs.getTimestamp("birthday").toLocalDateTime());
                        innerUser.setTickets(new HashSet<>());
                        return innerUser;
                    });

            List<Ticket> userTickets = (List) bookingRepository.getTicketsByUserEmail(email);
            userTickets.forEach(ticket -> user.getTickets().add(ticket));

            return user;
        } catch (Exception e) {
            log.warn("No such user in database");
        }
        return null;
    }

    @Override
    public User remove(String email) {

        User user = getByEmail(email);

        jdbcTemplate.update("DELETE FROM theatre_user WHERE email = ?",
                email);
        jdbcTemplate.update("DELETE FROM ticket WHERE user_email = ?",
                email);

        return user;
    }

    @Override
    public Collection<User> getAll() {

        List<String> userEmails = jdbcTemplate.query("SELECT email FROM theatre_user",
                (rs, rowNumber) -> rs.getString("email")
        );
        return userEmails.stream()
                .map(this::getByEmail)
                .collect(Collectors.toSet());
    }
}
