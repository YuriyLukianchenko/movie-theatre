package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.DiscountCounter;

public interface DiscountCounterAspectRepository {

    DiscountCounter getInformationAboutDiscount(String userEmail);

    void incrementMorningDiscountCounter(String userEmail);

    void incrementBirthdayStrategyCounter(String userEmail);

    void incrementAccumulatedDiscountCounter(String userEmail);
}
