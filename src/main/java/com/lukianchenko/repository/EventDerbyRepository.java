package com.lukianchenko.repository;

import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Repository("eventDerbyRepository")
public class EventDerbyRepository implements EventRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("auditoriumDerbyRepository")
    private AuditoriumRepository auditoriumRepository;

    @Override
    public void save(String name, Event event) {
        jdbcTemplate.update("INSERT INTO event (name, base_price, rating) VALUES(?, ?, ?)",
                name,
                event.getBasePrice(),
                event.getRating().toString());

        try {
            event.getAirDates().forEach((key, value) -> jdbcTemplate.update("INSERT INTO airdate (date_time, auditorium_name, event_name) VALUES(?, ?, ?)",
                    Timestamp.valueOf(key),
                    value.getName(),
                    name
            ));


        } catch (
                Exception e) {
            System.out.println("Such Airdate already exist");
        }
        System.out.println(

                getByName(name));
    }

    @Override
    public Event getByName(String name) {
        Event outterEvent = new Event();
        try {
            Event event = jdbcTemplate.queryForObject("SELECT * FROM event where name = ?",
                    new Object[]{name},
                    (rs, rowNumber) -> {
                        Event innerEvent = new Event();
                        innerEvent.setName(name);
                        innerEvent.setRating(Rating.valueOf(rs.getString("rating")));
                        innerEvent.setBasePrice(rs.getDouble("base_price"));
                        innerEvent.setAirDates(new HashMap<>());
                        return innerEvent;
                    }
            );


            List<Event> eventsWithAirDates = jdbcTemplate.query("SELECT * FROM airdate WHERE event_name = ?",
                    new Object[]{name},
                    (resultSet, i) -> {
                        Event currentEvent = new Event();
                        currentEvent.setAirDates(new HashMap<>());
                        currentEvent.getAirDates().put(
                                resultSet.getTimestamp("date_time").toLocalDateTime(),
                                auditoriumRepository.getByName(resultSet.getString("auditorium_name")));
                        return currentEvent;
                    });
            System.out.println("Event AirDAtes = " + eventsWithAirDates);
            eventsWithAirDates
                    .forEach(currentEvent -> event.getAirDates().put(currentEvent.getAirDates().entrySet().iterator().next().getKey(), currentEvent.getAirDates().entrySet().iterator().next().getValue()));

            outterEvent = event;

            return outterEvent;
        } catch (EmptyResultDataAccessException e) {
            System.out.println("Empty AirDAtes In Event");
        }

        return outterEvent;
    }

    @Override
    public Event remove(String name) {
        Event event = getByName(name);

        jdbcTemplate.update("DELETE FROM event WHERE name = ?",
                name);
        jdbcTemplate.update("DELETE FROM airdate WHERE event_name = ?",
                name);

        return event;
    }


    @Override
    public Collection<Event> getAll() {
        List<String> eventNames = jdbcTemplate.query("SELECT name FROM event",
                (rs, rowNumber) -> rs.getString("name")
        );
        return eventNames.stream()
                .map(this::getByName)
                .collect(Collectors.toSet());
    }

    @Override
    public void clear() {
        List<String> eventNames = jdbcTemplate.query("SELECT name FROM event",
                (rs, rowNumber) -> rs.getString("name")
        );
        eventNames.forEach(this::remove);
    }
}
