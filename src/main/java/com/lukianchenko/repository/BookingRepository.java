package com.lukianchenko.repository;

import com.lukianchenko.domain.Ticket;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Repository for ticket management.
 */
public interface BookingRepository {

    /**
     * Save ticket in repository.
     *
     * @param id     of ticket.
     * @param ticket need to be saved.
     */
    void save(String id, Ticket ticket);

    /**
     * Get all tickets for specific event on specific time.
     *
     * @param eventName .
     * @param time      of event.
     * @return all tickets for specific eventName and time.
     */
    Collection<Ticket> getTicketsByEventAndDateTime(String eventName, LocalDateTime time);

    /**
     * Get all tickets were booked by specific user.
     *
     * @param email of user.
     * @return all tickets user booked.
     */
    Collection<Ticket> getTicketsByUserEmail(String email);

    /**
     * Remove ticket from repository.
     *
     * @param id of the ticket.
     * @return removed ticket.
     */
    Ticket remove(String id);

    /**
     * Get all tickets from repository.
     * @return all tickets.
     */
    Collection<Ticket> getAll();

    /**
     * Remove all teakets in repository.
     */
    void clear();
}
