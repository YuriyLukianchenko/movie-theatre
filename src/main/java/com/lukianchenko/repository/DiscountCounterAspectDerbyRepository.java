package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.DiscountCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

@Repository("discountCounterAspectDerbyRepository")
public class DiscountCounterAspectDerbyRepository implements DiscountCounterAspectRepository {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void init() {
        jdbcTemplate.update("INSERT INTO discount_information_counter VALUES ('total', 0, 0, 0)");
    }

    @Override
    public DiscountCounter getInformationAboutDiscount(String eventName) {

        try {
            jdbcTemplate.queryForObject("SELECT event_name FROM discount_information_counter WHERE event_name = ?",
                    new Object[]{eventName}, String.class);
        } catch (EmptyResultDataAccessException e) {
            jdbcTemplate.update("INSERT INTO discount_information_counter VALUES (?, 0, 0, 0)",
                    eventName);
        }
        return jdbcTemplate.queryForObject("SELECT * FROM discount_information_counter WHERE event_name = ?",
                new Object[]{eventName},
                (rs, rowNumber) -> {
                    int morningDiscountCounter = rs.getInt("morning_discount_counter");
                    int accumulatedDiscountCounter = rs.getInt("accumulated_discount_counter");
                    int birthdayDiscountCounter = rs.getInt("birthday_discount_counter");

                    DiscountCounter discountCounter = new DiscountCounter();
                    discountCounter.setMorningDiscountCounter(morningDiscountCounter);
                    discountCounter.setAccumulatedDiscountCounter(accumulatedDiscountCounter);
                    discountCounter.setBirthdayDiscountCounter(birthdayDiscountCounter);

                    return discountCounter;
                });
    }

    @Override
    public void incrementMorningDiscountCounter(String eventName) {
        DiscountCounter discountCounter = getInformationAboutDiscount(eventName);
        int morningDiscountCounter = discountCounter.getMorningDiscountCounter();

        jdbcTemplate.update("UPDATE discount_information_counter SET morning_discount_counter = ? WHERE event_name = ?",
                ++morningDiscountCounter, eventName);
    }

    @Override
    public void incrementAccumulatedDiscountCounter(String eventName) {
        DiscountCounter discountCounter = getInformationAboutDiscount(eventName);
        int accumulatedDiscountCounter = discountCounter.getAccumulatedDiscountCounter();

        jdbcTemplate.update("UPDATE discount_information_counter SET accumulated_discount_counter = ? WHERE event_name = ?",
                ++accumulatedDiscountCounter, eventName);
    }

    @Override
    public void incrementBirthdayStrategyCounter(String eventName) {
        DiscountCounter discountCounter = getInformationAboutDiscount(eventName);
        int birthdayDiscountCounter = discountCounter.getBirthdayDiscountCounter();

        jdbcTemplate.update("UPDATE discount_information_counter SET birthday_discount_counter = ? WHERE event_name = ?",
                ++birthdayDiscountCounter, eventName);
    }
}
