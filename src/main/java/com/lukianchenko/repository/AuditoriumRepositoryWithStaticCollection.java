package com.lukianchenko.repository;

import com.lukianchenko.domain.Auditorium;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class AuditoriumRepositoryWithStaticCollection implements AuditoriumRepository {

    private static final Logger log =
            LogManager.getLogger(AuditoriumRepositoryWithStaticCollection.class);

    private static Map<String, Auditorium> auditoriums = new HashMap<>();

    @PostConstruct
    public void init() {
        getInformationAboutAuditories();
    }

    @Override
    public Auditorium getByName(String name) {
        return auditoriums.get(name);
    }

    @Override
    public Set<Auditorium> getAll() {
        return auditoriums.entrySet().stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    private void getInformationAboutAuditories() {
        Properties auditoriumProperties = new Properties();
        try (InputStream STREAM_TO_PROPERTIES_FILE = getClass().getClassLoader().getResourceAsStream("application.properties")){
            auditoriumProperties.load(STREAM_TO_PROPERTIES_FILE);

        Auditorium redAuditorium = new Auditorium();
        redAuditorium.setName(auditoriumProperties.getProperty("auditoriums.auditoriumRed.name"));
        redAuditorium.setNumberOfSeats(Integer.parseInt(auditoriumProperties.getProperty("auditoriums.auditoriumRed.numberOfSeats")));
        String[] vipSeatsString = auditoriumProperties.getProperty("auditoriums.auditoriumRed.vipSeats").split(",");
        redAuditorium.setVipSeats(Stream.of(vipSeatsString)
                .map(Long::parseLong)
                .collect(Collectors.toSet())
        );
        auditoriums.put(redAuditorium.getName(), redAuditorium);

        Auditorium greenAuditorium = new Auditorium();
        greenAuditorium.setName(auditoriumProperties.getProperty("auditoriums.auditoriumGreen.name"));
        greenAuditorium.setNumberOfSeats(Integer.parseInt(auditoriumProperties.getProperty("auditoriums.auditoriumGreen.numberOfSeats")));
        String[] vipSeatsStringGreen = auditoriumProperties.getProperty("auditoriums.auditoriumGreen.vipSeats").split(",");
        greenAuditorium.setVipSeats(Stream.of(vipSeatsStringGreen)
                .map(Long::parseLong)
                .collect(Collectors.toSet())
        );
        auditoriums.put(greenAuditorium.getName(), greenAuditorium);

        Auditorium blueAuditorium = new Auditorium();
        blueAuditorium.setName(auditoriumProperties.getProperty("auditoriums.auditoriumBlue.name"));
        blueAuditorium.setNumberOfSeats(Integer.parseInt(auditoriumProperties.getProperty("auditoriums.auditoriumBlue.numberOfSeats")));
        String[] vipSeatsStringBlue = auditoriumProperties.getProperty("auditoriums.auditoriumBlue.vipSeats").split(",");
        blueAuditorium.setVipSeats(Stream.of(vipSeatsStringBlue)
                .map(Long::parseLong)
                .collect(Collectors.toSet())
        );
        auditoriums.put(blueAuditorium.getName(), blueAuditorium);

        } catch (IOException e) {
            log.info("Failed to load properties");
        }
    }
}
