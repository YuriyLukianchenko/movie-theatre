package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.EventCounterInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("eventCounterAspectDerbyRepository")
public class EventCounterAspectDerbyRepository implements EventCounterAspectRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public EventCounterInformation getInformationAboutEvent(String eventName) {

        try {
            jdbcTemplate.queryForObject("SELECT event_name FROM event_information_counter WHERE event_name = ?",
                    new Object[]{eventName}, String.class);
        } catch (EmptyResultDataAccessException e) {
            jdbcTemplate.update("INSERT INTO event_information_counter VALUES (?, 0, 0, 0)",
                    eventName);
        }
        return jdbcTemplate.queryForObject("SELECT * FROM event_information_counter WHERE event_name = ?",
                new Object[]{eventName},
                (rs, rowNumber) -> {
                    int accessByNameCounter = rs.getInt("access_by_name_counter");
                    int priceQueriedCounter= rs.getInt("price_queried_counter");
                    int bookedTicketCounter = rs.getInt("booked_ticket_counter");

                    EventCounterInformation eventCounterInformation = new EventCounterInformation();
                    eventCounterInformation.setAccessedByNameNumber(accessByNameCounter);
                    eventCounterInformation.setPriceQueriedNumber(priceQueriedCounter);
                    eventCounterInformation.setBookedTicketNumber(bookedTicketCounter);

                    return eventCounterInformation;
                });
    }

    @Override
    public void incrementAccessByNameCounter(String eventName) {
        EventCounterInformation eventCounterInformation = getInformationAboutEvent(eventName);
        int accessedByNameNumber = eventCounterInformation.getAccessedByNameNumber();

        jdbcTemplate.update("UPDATE event_information_counter SET access_by_name_counter = ? WHERE event_name = ?",
                ++accessedByNameNumber, eventName);
    }

    @Override
    public void incrementPriceQueriedCounter(String eventName) {
        EventCounterInformation eventCounterInformation = getInformationAboutEvent(eventName);
        int priceQueriedNumber = eventCounterInformation.getPriceQueriedNumber();

        jdbcTemplate.update("UPDATE event_information_counter SET price_queried_counter = ? WHERE event_name = ?",
                ++priceQueriedNumber, eventName);
    }

    @Override
    public void incrementBookedTicketCounter(String eventName) {
        EventCounterInformation eventCounterInformation = getInformationAboutEvent(eventName);
        int bookedTicketNumber = eventCounterInformation.getBookedTicketNumber();

        jdbcTemplate.update("UPDATE event_information_counter SET booked_ticket_counter = ? WHERE event_name = ?",
                ++bookedTicketNumber, eventName);
    }

}
