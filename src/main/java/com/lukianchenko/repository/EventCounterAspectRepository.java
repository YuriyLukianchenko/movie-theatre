package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.EventCounterInformation;


public interface EventCounterAspectRepository {

    EventCounterInformation getInformationAboutEvent(String eventName);

    void incrementAccessByNameCounter(String eventName);

    void incrementPriceQueriedCounter(String eventName);

    void incrementBookedTicketCounter(String eventName);

}
