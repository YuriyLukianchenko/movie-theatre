package com.lukianchenko.repository;

import com.lukianchenko.domain.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class BookingDerbyRepository implements BookingRepository {

    private static final Logger log = LogManager.getLogger(UserDerbyRepository.class);

    private static final String SEAT_NUMBER = "seat_number";
    private static final String ID = "id";
    private static final String USER_EMAIL = "user_email";
    private static final String EVENT_NAME = "event_name";
    private static final String AUDITORIUM_NAME = "auditorium_name";
    private static final String DATE_TIME = "date_time";

    private JdbcTemplate jdbcTemplate;

    private EventRepository eventRepository;

    private AuditoriumRepository auditoriumRepository;

    @Autowired
    public BookingDerbyRepository(JdbcTemplate jdbcTemplate,
                                  @Qualifier("eventDerbyRepository")
                                          EventRepository eventRepository,
                                  @Qualifier("auditoriumDerbyRepository")
                                          AuditoriumRepository auditoriumRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.eventRepository = eventRepository;
        this.auditoriumRepository = auditoriumRepository;
    }

    @Override
    public void save(String id, Ticket ticket) {
        jdbcTemplate.update("INSERT INTO ticket VALUES (?, ?, ?, ?, ?, ?)",
                id,
                ticket.getUserEmail(),
                ticket.getEvent().getName(),
                ticket.getAuditorium().getName(),
                Timestamp.valueOf(ticket.getTime()),
                ticket.getSeatNumber());
    }

    @Override
    public Collection<Ticket> getTicketsByUserEmail(String email) {
        try {
            return jdbcTemplate.query("SELECT * FROM ticket WHERE user_email = ?",
                    new Object[]{email},
                    (rs, rowNumber) -> new Ticket(
                            rs.getString(USER_EMAIL),
                            eventRepository.getByName(rs.getString(EVENT_NAME)),
                            auditoriumRepository.getByName(rs.getString(AUDITORIUM_NAME)),
                            rs.getTimestamp(DATE_TIME).toLocalDateTime(),
                            rs.getInt(SEAT_NUMBER)
                    ));
        } catch (Exception e) {
            log.warn("Such user is absent in database");
        }
        return Collections.emptyList();
    }

    @Override
    public Collection<Ticket> getTicketsByEventAndDateTime(String eventName, LocalDateTime time) {
        return jdbcTemplate.query("SELECT * FROM ticket WHERE event_time = ? AND date_time = ?",
                new Object[]{eventName, Timestamp.valueOf(time)},
                (rs, rowNumber) -> new Ticket(
                        rs.getString(USER_EMAIL),
                        eventRepository.getByName(rs.getString(EVENT_NAME)),
                        auditoriumRepository.getByName(rs.getString(AUDITORIUM_NAME)),
                        rs.getTimestamp(DATE_TIME).toLocalDateTime(),
                        rs.getInt(SEAT_NUMBER)
                ));
    }

    @Override
    public Ticket remove(String id) {
        Ticket ticket = jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id = ?",
                new Object[]{id},
                (rs, rowNumber) -> new Ticket(
                        rs.getString(USER_EMAIL),
                        eventRepository.getByName(rs.getString(EVENT_NAME)),
                        auditoriumRepository.getByName(rs.getString(AUDITORIUM_NAME)),
                        rs.getTimestamp(DATE_TIME).toLocalDateTime(),
                        rs.getInt(SEAT_NUMBER)));
        jdbcTemplate.update("DELETE FROM ticket WHERE id = ?",
                id);
        return ticket;
    }

    @Override
    public Collection<Ticket> getAll() {
        return jdbcTemplate.query("SELECT * FROM ticket",
                (rs, rowNumber) -> new Ticket(
                        rs.getString(USER_EMAIL),
                        eventRepository.getByName(rs.getString(EVENT_NAME)),
                        auditoriumRepository.getByName(rs.getString(AUDITORIUM_NAME)),
                        rs.getTimestamp(DATE_TIME).toLocalDateTime(),
                        rs.getInt(SEAT_NUMBER)));
    }

    @Override
    public void clear() {
        List<String> ids = jdbcTemplate.query("SELECT id FROM ticket",
                (rs, rowNumber) -> rs.getString(ID)
        );
        ids.stream().forEach(this::remove);
    }
}
