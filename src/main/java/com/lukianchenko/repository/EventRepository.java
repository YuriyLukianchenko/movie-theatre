package com.lukianchenko.repository;

import com.lukianchenko.domain.Event;

import java.util.Collection;

/**
 * Repository for event management.
 */
public interface EventRepository {

    /**
     * Save event to repository.
     *
     * @param name  of event need to be saved.
     * @param event need to be saved.
     */
    void save(String name, Event event);

    /**
     * Get event by name.
     *
     * @param name of the event.
     * @return event with specific name.
     */
    Event getByName(String name);

    /**
     * Remove event from repository.
     *
     * @param name of event need to be removed.
     * @return removed event.
     */
    Event remove(String name);

    /**
     * Get all events from repository.
     *
     * @return collection of all events from repository.
     */
    Collection<Event> getAll();

    /**
     * Clear repository, delete all events from repository.
     */
    void clear();
}
