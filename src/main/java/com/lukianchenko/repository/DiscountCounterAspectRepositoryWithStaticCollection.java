package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.DiscountCounter;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DiscountCounterAspectRepositoryWithStaticCollection implements DiscountCounterAspectRepository {
    
    private static Map<String, DiscountCounter> discountCounter = new HashMap<>();

    @PostConstruct
    private void init() {
        discountCounter.put("total", new DiscountCounter());
    }

    @Override
    public DiscountCounter getInformationAboutDiscount(String userEmail) {
        return discountCounter.get(userEmail);
    }
    @Override
    public void incrementMorningDiscountCounter(String userEmail) {
        if (discountCounter.get(userEmail) == null) {
            discountCounter.put(userEmail, new DiscountCounter());
        }
        int counter = discountCounter.get(userEmail).getMorningDiscountCounter();
        discountCounter.get(userEmail).setMorningDiscountCounter(++counter);

        int totalCounter = discountCounter.get("total").getMorningDiscountCounter();
        discountCounter.get("total").setMorningDiscountCounter(++totalCounter);
    }
    @Override
    public void incrementBirthdayStrategyCounter(String userEmail) {
        if (discountCounter.get(userEmail) == null) {
            discountCounter.put(userEmail, new DiscountCounter());
        }
        int counter = discountCounter.get(userEmail).getBirthdayDiscountCounter();
        discountCounter.get(userEmail).setBirthdayDiscountCounter(++counter);

        int totalCounter = discountCounter.get("total").getBirthdayDiscountCounter();
        discountCounter.get("total").setBirthdayDiscountCounter(++totalCounter);
    }
    @Override
    public void incrementAccumulatedDiscountCounter(String userEmail) {
        if (discountCounter.get(userEmail) == null) {
            discountCounter.put(userEmail, new DiscountCounter());
        }
        int counter = discountCounter.get("total").getAccumulatedDiscountCounter();
        discountCounter.get("total").setAccumulatedDiscountCounter(++counter);
    }
}
