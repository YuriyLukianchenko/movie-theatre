package com.lukianchenko.repository;

import com.lukianchenko.domain.aspect.EventCounterInformation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class EventCounterAspectRepositoryWithStaticMap implements EventCounterAspectRepository {

    private static Map<String, EventCounterInformation> eventInfoCounter = new HashMap<>();

    @Override
    public EventCounterInformation getInformationAboutEvent(String eventName) {
        return eventInfoCounter.get(eventName);
    }
    @Override
    public void incrementAccessByNameCounter(String eventName) {
        if (eventInfoCounter.get(eventName) == null) {
            eventInfoCounter.put(eventName, new EventCounterInformation());
        }
        int counter = eventInfoCounter.get(eventName).getAccessedByNameNumber();
        eventInfoCounter.get(eventName).setAccessedByNameNumber(++counter);
    }
    @Override
    public void incrementPriceQueriedCounter(String eventName) {
        if (eventInfoCounter.get(eventName) == null) {
            eventInfoCounter.put(eventName, new EventCounterInformation());
        }
        int counter = eventInfoCounter.get(eventName).getPriceQueriedNumber();
        eventInfoCounter.get(eventName).setPriceQueriedNumber(++counter);
    }
    @Override
    public void incrementBookedTicketCounter(String eventName) {
        if (eventInfoCounter.get(eventName) == null) {
            eventInfoCounter.put(eventName, new EventCounterInformation());
        }
        int counter = eventInfoCounter.get(eventName).getBookedTicketNumber();
        eventInfoCounter.get(eventName).setBookedTicketNumber(++counter);
    }

}
