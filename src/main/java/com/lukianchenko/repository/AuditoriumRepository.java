package com.lukianchenko.repository;


import com.lukianchenko.domain.Auditorium;

import java.util.Set;

/**
 * Repository for Auditorium management.
 */
public interface AuditoriumRepository {

    /**
     * Get auditorium by its name.
     *
     * @param name of Auditorium.
     * @return auditorium.
     */
    Auditorium getByName(String name);

    /**
     * Get all auditoriums.
     *
     * @return all auditoriums in repository.
     */
    Set<Auditorium> getAll();

}
