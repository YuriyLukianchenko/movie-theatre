package com.lukianchenko.repository;

import com.lukianchenko.domain.Auditorium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Repository("auditoriumDerbyRepository")
public class AuditoriumDerbyRepository implements AuditoriumRepository {

    private static final String NAME = "name";
    private static final String NUMBER_OF_SEATS = "number_of_seats";
    private static final String VIP_SEATS_ARRAY_STRING = "vip_seats_array_string";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Auditorium getByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM auditorium WHERE name = ?",
                new Object[]{name},
                (rs, rowNumber) -> {
                    String name1 = rs.getString(NAME);
                    int numberOfSeats = rs.getInt(NUMBER_OF_SEATS);
                    String[] vipSeatsString = rs.getString(VIP_SEATS_ARRAY_STRING).split(",");

                    Set<Long> vipSeats = Stream.of(vipSeatsString)
                            .map(Long::parseLong)
                            .collect(Collectors.toSet());
                    return new Auditorium(name1, numberOfSeats, vipSeats);
                });
    }

    public Set<Auditorium> getAll() {
        return (Set) jdbcTemplate.query("SELECT * FROM auditorium",
                (rs, rowNumber) -> {
                   String name = rs.getString(NAME);
                   int numberOfSeats = rs.getInt(NUMBER_OF_SEATS);
                   String[] vipSeatsString = rs.getString(VIP_SEATS_ARRAY_STRING).split(",");

                   Set<Long> vipSeats = Stream.of(vipSeatsString)
                           .map(Long::parseLong)
                           .collect(Collectors.toSet());

                   return new Auditorium(name, numberOfSeats, vipSeats);
               });
    }
}
