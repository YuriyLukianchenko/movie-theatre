package com.lukianchenko.repository;

import com.lukianchenko.domain.User;

import java.util.Collection;

/**
 * Repository for user management
 */
public interface UserRepository {

    /**
     * Save user to repository.
     *
     * @param email of the user need to be saved.
     * @param user  need to be saved to repository.
     */
    void save(String email, User user);

    /**
     * Get user by its email.
     *
     * @param email of the user.
     * @return user with specific email.
     */
    User getByEmail(String email);

    /**
     * Remove user from repository.
     *
     * @param email of the user.
     * @return removed user.
     */
    User remove(String email);

    /**
     * Get all users from the repository.
     *
     * @return all users from repository.
     */
    Collection<User> getAll();
}
