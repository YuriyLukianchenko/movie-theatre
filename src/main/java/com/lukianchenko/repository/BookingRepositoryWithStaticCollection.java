package com.lukianchenko.repository;

import com.lukianchenko.domain.Ticket;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class BookingRepositoryWithStaticCollection implements BookingRepository {

    private static Map<String, Ticket> tickets = new HashMap<>();

    @Override
    public void save(String id, Ticket ticket) {
        tickets.put(id, ticket);
    }

    @Override
    public Collection<Ticket> getTicketsByEventAndDateTime(String eventName, LocalDateTime time) {
        return tickets.values().stream()
                .filter(ticket -> ticket.getEvent().getName().equals(eventName))
                .filter(ticket -> ticket.getTime().equals(time))
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<Ticket> getTicketsByUserEmail(String email) {
        return tickets.values().stream()
                .filter(ticket -> ticket.getUserEmail().equals(email))
                .collect(Collectors.toSet());
    }

    @Override
    public Ticket remove(String id) {
        return tickets.remove(id);
    }


    @Override
    public Collection<Ticket> getAll() {
        return tickets.values();
    }
    @Override
    public void clear() {
        tickets.clear();
    }
}
