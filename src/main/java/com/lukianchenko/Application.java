package com.lukianchenko;

import com.lukianchenko.view.ConsoleView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext();
        ((AnnotationConfigApplicationContext) ctx).scan("com.lukianchenko");
        ((AnnotationConfigApplicationContext) ctx).refresh();
        ConsoleView view = (ConsoleView) ctx.getBean("consoleView");
        view.run();
    }
}
