package com.lukianchenko.aspect;

import com.lukianchenko.repository.DiscountCounterAspectRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class DiscountAspect {

    private DiscountCounterAspectRepository discountCounterAspectRepository;

    private static String  currentUserEmail = "";

    @Autowired
    public DiscountAspect(
            @Qualifier("discountCounterAspectDerbyRepository")
            DiscountCounterAspectRepository discountCounterAspectRepository) {
        this.discountCounterAspectRepository = discountCounterAspectRepository;
    }

    @Pointcut("execution(double *.getPrice(..))")
    public void setCurrentUserEmail(){}

    @Pointcut("execution(double *.calculateDiscount(..))")
    private void calculateDiscountCounter(){}

    @Pointcut("calculateDiscountCounter()" +
            "&& target(com.lukianchenko.service.discount.MorningStrategy)")
    private void calculateMorningDiscountCounter(){}

    @Pointcut("calculateDiscountCounter()" +
            " && target(com.lukianchenko.service.discount.AccumulatedStrategy)")
    private void calculateAccumulatedDiscountCounter(){}

    @Pointcut("calculateDiscountCounter()" +
            " && target(com.lukianchenko.service.discount.BirthDayStrategy)")
    private void calculateBirthdayDiscountCounter(){}


    @Before("setCurrentUserEmail()")
    public void afterGetPrice(JoinPoint jp) {
        currentUserEmail = jp.getArgs()[2].toString();
    }

    @AfterReturning("calculateMorningDiscountCounter()")
    public void afterMorningStrategy() {
        discountCounterAspectRepository.incrementMorningDiscountCounter(currentUserEmail);
    }

    @AfterReturning("calculateBirthdayDiscountCounter()")
    public void afterBirthdayStrategy() {
        discountCounterAspectRepository.incrementBirthdayStrategyCounter(currentUserEmail);
    }

    @AfterReturning("calculateAccumulatedDiscountCounter()")
    public void afterAccumulatedStrategy() {
        discountCounterAspectRepository.incrementAccumulatedDiscountCounter(currentUserEmail);
    }
}
