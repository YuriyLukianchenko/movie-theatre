package com.lukianchenko.aspect;

import com.lukianchenko.domain.Ticket;
import com.lukianchenko.repository.EventCounterAspectDerbyRepository;
import com.lukianchenko.repository.EventCounterAspectRepository;
import com.lukianchenko.repository.EventCounterAspectRepositoryWithStaticMap;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Set;

@Aspect
@Component
public class CounterAspect {

    private EventCounterAspectRepository eventCounterAspectRepository;

    @Autowired
    public CounterAspect(
            @Qualifier("eventCounterAspectDerbyRepository")
            EventCounterAspectDerbyRepository counterAspectRepository) {
            this.eventCounterAspectRepository = counterAspectRepository;
    }

    @Pointcut("execution(com.lukianchenko.domain.Event *.getByName(..))")
    private void byNameEventAccessCounter(){}

    @AfterReturning("byNameEventAccessCounter()")
    private void afterGetByName(JoinPoint jp) {
        String eventName = jp.getArgs()[0].toString();
        eventCounterAspectRepository.incrementAccessByNameCounter(eventName);
    }

    @Pointcut("execution(* *.getPrice(..))")
    private void getEventPriceCounter(){}

    @AfterReturning("getEventPriceCounter()")
    private void afterGetEventPrice(JoinPoint jp) {
        String eventName = jp.getArgs()[0].toString();
        eventCounterAspectRepository.incrementPriceQueriedCounter(eventName);
    }

    @Pointcut("execution(* *.bookTickets(..))")
    private void bookTicketsCounter(){}

    @AfterReturning("bookTicketsCounter()")
    private void afterbookTickets(JoinPoint jp) {
        Set<Ticket> tickets = (Set) jp.getArgs()[0];
        String eventName = tickets.iterator().next().getEvent().getName();
        eventCounterAspectRepository.incrementBookedTicketCounter(eventName);
    }
}
