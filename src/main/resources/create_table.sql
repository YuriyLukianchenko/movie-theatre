CREATE TABLE auditorium (
  name VARCHAR(30) PRIMARY KEY,
  number_of_seats  INTEGER,
  vip_seats_array_string VARCHAR(50)
);

CREATE TABLE event_information_counter (
  event_name VARCHAR(30) PRIMARY KEY,
  access_by_name_counter INTEGER,
  price_queried_counter INTEGER,
  booked_ticket_counter INTEGER
);

CREATE TABLE discount_information_counter (
  event_name VARCHAR(30) PRIMARY KEY,
  morning_discount_counter INTEGER,
  accumulated_discount_counter INTEGER,
  birthday_discount_counter INTEGER
);
CREATE TABLE event (
  id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY,
  name VARCHAR(15) NOT NULL UNIQUE,
  base_price DOUBLE NOT NULL,
  rating VARCHAR(15) NOT NULL
);

CREATE TABLE airdate (
  id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  date_time TIMESTAMP NOT NULL,
  auditorium_name VARCHAR(30) NOT NULL,
  event_name VARCHAR(15) NOT NULL,

  PRIMARY KEY (date_time, auditorium_name)
);

CREATE TABLE theatre_user (
  email VARCHAR(30) PRIMARY KEY,
  admin VARCHAR(5) NOT NULL,
  birthday TIMESTAMP NOT NULL
);

CREATE TABLE ticket (
  id VARCHAR(50) NOT NULL PRIMARY KEY,
  user_email VARCHAR(30) NOT NULL,
  event_name VARCHAR(15) NOT NULL,
  auditorium_name VARCHAR(30) NOT NULL,
  date_time TIMESTAMP NOT NULL,
  seat_number INTEGER NOT NULL
);

