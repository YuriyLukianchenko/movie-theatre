package com.lukianchenko.service.discount;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BirthdayStrategyTest extends DiscountStrategyTest {

    @BeforeEach
    private void init() {
        discountStrategy = new BirthDayStrategy();
    }

    @Test
    void calculateDiscount_whenBirthdayIsWithin5Days_thenBirthdayDiscountIsActivated() {
        assertTrue(discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay) > 0);
    }

    @Test
    void calculateDiscount_whenBirthdayIsNotWithin5Days_thenBirthdayDiscountIsNotActivated() {
        birthDay = birthDay.plusDays(2);
        assertEquals(0, discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay));
    }
}
