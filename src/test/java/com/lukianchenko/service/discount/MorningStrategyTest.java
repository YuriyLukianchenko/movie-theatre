package com.lukianchenko.service.discount;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MorningStrategyTest extends DiscountStrategyTest {

    @BeforeEach
    private void init() {
        discountStrategy = new MorningStrategy();
    }

    @Test
    void calculateDiscount_whenAirTimeBefore12AM_thenMorningDiscountIsActivated() {
        assertTrue(discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay) > 0);
    }

    @Test
    void calculateDiscount_whenAirTimeAfter12AM_thenMorningDiscountIsNotActivated() {
        airTime = airTime.plusHours(3);
        assertEquals(0, discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay));
    }
}
