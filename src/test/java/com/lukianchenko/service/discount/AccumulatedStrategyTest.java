package com.lukianchenko.service.discount;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccumulatedStrategyTest extends DiscountStrategyTest {

    @BeforeEach
    private void init() {
        discountStrategy = new AccumulatedStrategy();
    }

    @Test
    void calculateDiscount_whenNumberOfTicketsEquals1_thenAccumulatedStrategyIsNotActivated() {
        numberOfTickets = 1;
        assertEquals(0, discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay));
    }

    @Test
    void calculateDiscount_whenNumberOfTicketsMoreThan1_thenAccumulatedStrategyIsActivated() {

        assertTrue(discountStrategy.calculateDiscount(numberOfTickets, airTime, birthDay) > 0);
    }
}
