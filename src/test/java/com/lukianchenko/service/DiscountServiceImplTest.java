package com.lukianchenko.service;

import com.lukianchenko.service.discount.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DiscountServiceImplTest {

    private DiscountServiceImpl discountService = new DiscountServiceImpl();

    private int numberOfTickets = 10;
    private LocalDateTime birthDay =
            LocalDateTime.of(2000, 5, 18, 10, 0);
    private LocalDateTime airTime =
            LocalDateTime.of(2020, 5, 10, 10, 0);

    @BeforeEach
    private void init() {
        DiscountStrategy birthdayStrategy = new BirthDayStrategy();
        DiscountStrategy morningStrategy = new MorningStrategy();
        DiscountStrategy accumulatedStrategy = new AccumulatedStrategy();

        Set<DiscountStrategy> strategies = Set.of(birthdayStrategy,
                morningStrategy,
                accumulatedStrategy
        );
        discountService.setDiscountStrategies(strategies);
    }

    @Test
    void getDiscount_whenMorningAirTimeAndNumberOfTicketsEqualsTo10AndNotBirthday_thenMorningDiscountIsReturned() {
        assertEquals(10, discountService.getDiscount(numberOfTickets,
                airTime,
                birthDay));
    }

    @Test
    void getDiscount_whenAfterMorningAirTimeAndNumberOfTicketsEqualsTo10AndNotBirthday_thenAccumulatedDiscountIsReturned() {
        airTime = airTime.plusHours(3);
        assertTrue(discountService.getDiscount(numberOfTickets, airTime, birthDay) > 0);
    }

    @Test
    void getDiscount_whenMorningAirTimeAndNumberOfTicketsEqualsTo10AndBirthday_thenBirthdayDiscountIsReturned() {

        birthDay = birthDay.minusDays(4);
        assertEquals(20, discountService.getDiscount(numberOfTickets,
                airTime,
                birthDay));
    }
}
