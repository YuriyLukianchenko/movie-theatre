package com.lukianchenko.service;

import com.lukianchenko.domain.Event;
import com.lukianchenko.domain.Rating;
import com.lukianchenko.repository.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

class EventServiceImplTest {

    @Mock
    private EventRepository eventRepository;

    @InjectMocks
    private EventServiceImpl eventService;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createEvent_whenNewEventIsCreated_thenEventRepositoryInvokeSaveMethod() {
        eventService.createEvent("Lord of the rings", 37.34, Rating.MEDIUM);
        verify(eventRepository).save(eq("Lord of the rings"), any(Event.class));
    }

    @Test
    void createEvent_whenAlreadyExistedEventIsCreated_thenEventRepositoryDoesNotInvokeSaveMethod() {
        Event event = new Event("Lord of the rings", 37.34, Rating.MEDIUM);

        when(eventRepository.getAll()).thenReturn(Collections.emptyList()).thenReturn(Set.of(event));
        eventService.createEvent("Lord of the rings", 37.34, Rating.MEDIUM);
        eventService.createEvent("Lord of the rings", 35.34, Rating.HIGH);

        verify(eventRepository, times(1)).save(eq("Lord of the rings"), any(Event.class));
    }

    @Test
    void getByName_whenGetByName_thenReturnEventWithSameName() {
        Event event = new Event("Lord of the rings", 37.34, Rating.MEDIUM);
        when(eventRepository.getByName("Lord of the rings")).thenReturn(event).thenReturn(null);

        assertEquals(event, eventService.getByName("Lord of the rings"));
        assertNull(eventService.getByName("Lord of the rings"));
    }


    @Test
    void remove_whenEventIsRemoved_thenEventRepositoryInvokesRemoveMethod() {
        Event event = new Event("Lord of the rings", 37.34, Rating.MEDIUM);
        when(eventRepository.remove("Lord of the rings")).thenReturn(event).thenReturn(null);
        eventService.remove("Lord of the rings");
        eventService.remove("Lord of the rings");
        verify(eventRepository, times(2)).remove("Lord of the rings");
    }

    @Test
    void getAll_whenGetAllIsInvoked_thenEventRepositoryInvokesGetAllMethod() {
        Event event1 = new Event("Lord of the rings", 37.34, Rating.HIGH);
        Event event2 = new Event("Harry Potter", 35.34, Rating.HIGH);

        when(eventRepository.getAll()).thenReturn(Set.of(event1, event2)).thenReturn(Collections.emptyList());

        assertEquals(2, eventService.getAll().size());
        assertEquals(0, eventService.getAll().size());

        verify(eventRepository, times(2)).getAll();
    }

    @Test
    void updateEventRating_whenUpdateEventRatingIsCalled_thenEventContainsUpdatedRating() {
        Event event1 = new Event("Lord of the rings", 37.34, Rating.HIGH);

        when(eventRepository.getByName("Lord of the rings")).thenReturn(event1).thenReturn(null);
        eventService.updateEventRating("Lord of the rings",Rating.MEDIUM);

        assertEquals(Rating.MEDIUM, event1.getRating());
        eventService.updateEventRating("Lord of the rings",Rating.MEDIUM);
        verify(eventRepository, times(2)).getByName("Lord of the rings");

    }

    @Test
    void updateEventBasePrice_whenUpdateEventBasePriceIsCalled_thenEventContainsUpdatedBasePrice() {
        Event event1 = new Event("Lord of the rings", 37.34, Rating.HIGH);

        when(eventRepository.getByName("Lord of the rings")).thenReturn(event1).thenReturn(null);

        eventService.updateEventBasePrice("Lord of the rings",40.45);
        assertEquals(40.45, event1.getBasePrice());

        eventService.updateEventBasePrice("Lord of the rings",10.11);
        verify(eventRepository, times(2)).getByName("Lord of the rings");

    }

}
