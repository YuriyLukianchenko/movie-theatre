package com.lukianchenko.service;

import com.lukianchenko.domain.Ticket;
import com.lukianchenko.domain.User;
import com.lukianchenko.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {
    
    @Mock
    private UserRepository userRepository;
    
    @InjectMocks
    private UserServiceImpl userService;
    
    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createUser_whenNewUserIsCreated_thenUserRepositoryInvokeSaveMethod() {
        userService.createUser("Lord@gmail.com",false, LocalDateTime.of(2000,1,1,1,1,1));
        verify(userRepository).save(eq("Lord@gmail.com"), any(User.class));
    }

    @Test
    void createUser_whenAlreadyExistedUserIsCreated_thenUserRepositoryDoesNotInvokeSaveMethod() {
        User user = new User("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1), Collections.emptySet());

        when(userRepository.getAll()).thenReturn(Collections.emptyList()).thenReturn(Set.of(user));
        userService.createUser("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1));
        userService.createUser("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1));

        verify(userRepository, times(1)).save(eq("Lord@gmail.com"), any(User.class));
    }

    @Test
    void getByEmail_whenGetByEmail_thenReturnUserWithSameEmail() {
        User user = new User("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1),Collections.emptySet());
        when(userRepository.getByEmail("Lord@gmail.com")).thenReturn(user).thenReturn(null);

        assertEquals(user, userService.getByEmail("Lord@gmail.com"));
        assertNull(userService.getByEmail("Lord@gmail.com"));
    }


    @Test
    void remove_whenUserIsRemoved_thenUserRepositoryInvokesRemoveMethod() {
        User user = new User("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1), Collections.emptySet());
        when(userRepository.remove("Lord@gmail.com")).thenReturn(user).thenReturn(null);
        userService.remove("Lord@gmail.com");
        userService.remove("Lord@gmail.com");
        verify(userRepository, times(2)).remove("Lord@gmail.com");
    }

    @Test
    void getAll_whenGetAllIsInvoked_thenUserRepositoryInvokesGetAllMethod() {
        User user1 = new User("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1), Collections.emptySet());
        User user2 = new User("Harry@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1), Collections.emptySet());

        when(userRepository.getAll()).thenReturn(Set.of(user1, user2)).thenReturn(Collections.emptyList());

        assertEquals(2, userService.getAll().size());
        assertEquals(0, userService.getAll().size());

        verify(userRepository, times(2)).getAll();
    }

    @Test
    void updateUserRole_whenUpdateUserRoleIsCalled_thenUserContainsUpdatedRole() {
        User user1 = new User("Lord@gmail.com", true, LocalDateTime.of(2000,1,1,1,1,1), Collections.emptySet());

        when(userRepository.getByEmail("Lord@gmail.com")).thenReturn(user1).thenReturn(null);
        userService.updateUserRole("Lord@gmail.com",false);

        assertFalse(user1.isAdmin());
        userService.updateUserRole("Lord@gmail.com",true);
        verify(userRepository, times(2)).getByEmail("Lord@gmail.com");

    }

    @Test
    void addUserTickets_whenAddNewTicket_thenThisTicketApearInUserTickets() {

        Ticket ticket1 = new Ticket();

        userService.addUserTicket("Lord@gmail.com", ticket1);

        verify(userRepository, times(1)).getByEmail("Lord@gmail.com");
    }
}
