package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.repository.AuditoriumRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class AuditoriumServiceImplTest {

    @Mock
    private AuditoriumRepository auditoriumRepository;

    @InjectMocks
    private AuditoriumServiceImpl auditoriumService;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getByName_whenInvokeMethodWithRedHallParameter_thenAuditoriumWithSameNameReturned() {
        String name = "BlueHall";
        Auditorium blueAuditorium = new Auditorium();
        blueAuditorium.setName(name);

        when(auditoriumRepository.getByName(name)).thenReturn(blueAuditorium);

        assertEquals(name, auditoriumService.getByName(name).getName());
    }

    @Test
    void getAll_whenInvokeMethod_thenReturnedCollectionSizeEqualsTo3() {
        Set<Auditorium> auditoriums = Set.of(new Auditorium(), new Auditorium(), new Auditorium());
        when(auditoriumRepository.getAll()).thenReturn(auditoriums);
        assertEquals(3, auditoriumService.getAll().size());
    }
}
