package com.lukianchenko.service;

import com.lukianchenko.domain.Auditorium;
import com.lukianchenko.domain.Rating;
import com.lukianchenko.domain.Ticket;
import com.lukianchenko.repository.*;
import com.lukianchenko.service.discount.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class BookingServiceTest {

    private EventService eventService;

    private UserService userService;

    private BookingService bookingService;

    private AuditoriumService auditoriumService;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        createDefaultMovieTheatreInformation();
    }

    @Test
    void checkAndCreateTickets_whenAllDataAreValid_then3TicketsAreCreated() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 0);
        Set<Integer> seats = Set.of(3, 4, 5);
        Set<Ticket> tickets = bookingService.createTickets("KZK",
                airTime,
                "Bobi@gail.com",
                seats);

        assertEquals(3, tickets.size());
    }

    @Test
    void checkAndCreateTickets_whenAirDateIsInvalid_then0TicketsAreCreated() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 10);
        Set<Integer> seats = Set.of(3, 4, 5);
        Set<Ticket> tickets = bookingService.createTickets("KZK",
                airTime,
                "Bobi@gail.com",
                seats);

        assertNull(tickets);
    }

    @Test
    void checkAndCreateTickets_whenSeatIsInvalid_then0TicketsAreCreated() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 0);
        Set<Integer> seats = Set.of(3, 4, 201);
        Set<Ticket> tickets = bookingService.createTickets("KZK",
                airTime,
                "Bobi@gail.com",
                seats);

        assertNull(tickets);
    }

    @Test
    void checkAndCreateTickets_whenEventNameIsInvalid_then0TicketsAreCreated() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 0);
        Set<Integer> seats = Set.of(3, 4, 5);
        Set<Ticket> tickets = bookingService.createTickets("KZK2",
                airTime,
                "Bobi@gail.com",
                seats);

        assertNull(tickets);
    }

    @Test
    void getPrice_whenAllDataAreValid_thenCorrectPriceIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(3);
        double price = bookingService.getPrice("KZK",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(55, price);
    }

    @Test
    void getPrice_whenMorningTime_thenPriceWithMorningDiscountIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 4, 10, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(3);
        double price = bookingService.getPrice("KZK",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(49.5, price);
    }

    @Test
    void getPrice_whenMoreThenOneTicketIsBought_thenPriceWithAccumulatedDiscountIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 2, 18, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(3, 2);
        double price = bookingService.getPrice("KZK",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertTrue(price > 109.235 || price < 109.24);
    }

    @Test
    void getPrice_whenTicketIsBoughtOnBirthday_thenPriceWithBirthdayDiscountIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 2, 18, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 3, 0, 0);

        Set<Integer> seats = Set.of(3);
        double price = bookingService.getPrice("KZK",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(44, price);
    }

    @Test
    void getPrice_whenVIPTicketIsBought_thenIncreasedPriceIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 2, 18, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(47);
        double price = bookingService.getPrice("KZK",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(110, price);
    }

    @Test
    void getPrice_whenTicketOnHighRatedMovieIsBought_thenIncreasedPriceIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 23, 0);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(3);
        double price = bookingService.getPrice("StarCat",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(108, price);
    }

    @Test
    void getPrice_whenTicketOnLowRatedMovieIsBought_thenDecreasedPriceIsReturned() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 14, 30);
        LocalDateTime birthday = LocalDateTime.of(2020, 6, 21, 0, 0);

        Set<Integer> seats = Set.of(3);
        double price = bookingService.getPrice("VanKycios",
                airTime,
                "Bobi@gail.com",
                seats,
                birthday);

        assertEquals(8.40, price);
    }

    @Test
    void bookAndGetTickets_whenTicketsAreBookedAndGetPurchasedTickets_thenReturnedBookedTickets() {
        LocalDateTime airTime = LocalDateTime.of(2020, 6, 1, 15, 0);
        Set<Integer> seats = Set.of(3, 4, 5);
        Set<Ticket> tickets = bookingService.createTickets("KZK",
                airTime,
                "Bobi@gail.com",
                seats);
        bookingService.bookTickets(tickets);
        Set<Ticket> bookedTickets = bookingService.getPurchasedTicketsForEvent(eventService.getByName("KZK"), airTime);

        assertEquals(tickets, bookedTickets);
    }


    private void createDefaultMovieTheatreInformation() {

        DiscountStrategy birthdayStrategy = new BirthDayStrategy();
        DiscountStrategy accumulatedStrategy = new AccumulatedStrategy();
        DiscountStrategy morningStrategy = new MorningStrategy();

        Set<DiscountStrategy> strategies = Set.of(birthdayStrategy, accumulatedStrategy, morningStrategy);


        AuditoriumRepository auditoriumRepository = new AuditoriumRepositoryWithStaticCollection();
        ((AuditoriumRepositoryWithStaticCollection) auditoriumRepository).init();
        auditoriumService = new AuditoriumServiceImpl(auditoriumRepository);
        eventService = new EventServiceImpl(new EventRepositoryWithStaticCollection(), new AuditoriumServiceImpl(auditoriumRepository));
        userService = new UserServiceImpl(new UserRepositoryWithStaticCollection());
        bookingService = new BookingServiceImpl(new BookingRepositoryWithStaticCollection(), eventService, userService, new DiscountServiceImpl(strategies));

        Auditorium blueAuditorium = auditoriumService.getByName("BlueHall");
        Auditorium redAuditorium = auditoriumService.getByName("RedHall");
        Auditorium greenAuditorium = auditoriumService.getByName("GreenHall");

        eventService.createEvent("KZK", 55, Rating.MEDIUM);
        eventService.updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 1, 15, 0),
                blueAuditorium);
        eventService.updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 2, 18, 0),
                blueAuditorium);
        eventService.updateEventAirDates("KZK",
                LocalDateTime.of(2020, 6, 4, 10, 0),
                blueAuditorium);

        eventService.createEvent("StarCat", 90, Rating.HIGH);
        eventService.updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 1, 23, 0),
                redAuditorium);
        eventService.updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 3, 16, 20),
                redAuditorium);
        eventService.updateEventAirDates("StarCat",
                LocalDateTime.of(2020, 6, 5, 11, 30),
                blueAuditorium);

        eventService.createEvent("VanKycios", 10.50, Rating.LOW);
        eventService.updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 5, 31, 9, 40),
                greenAuditorium);
        eventService.updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 6, 1, 14, 30),
                greenAuditorium);
        eventService.updateEventAirDates("VanKycios",
                LocalDateTime.of(2020, 6, 2, 23, 30),
                redAuditorium);
    }
}