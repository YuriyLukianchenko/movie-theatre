package com.lukianchenko.repository;

import com.lukianchenko.domain.Ticket;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookingrepositoryWithStaticCollectionTest {

    private BookingRepository bookingRepository = new BookingRepositoryWithStaticCollection();

    @Test
    void saveAndGetByUserEmail_whenNewTicketIsSaved_thenBookingRepositoryContainsThisTicket() {
        Ticket ticket = new Ticket();
        ticket.setUserEmail("Blade@gmail.com");
        bookingRepository.save(ticket.getId().toString(), ticket);
        assertEquals(Set.of(ticket), bookingRepository.getTicketsByUserEmail("Blade@gmail.com"));
    }

    @Test
    void getAll_whenTwoElementsAreSaved_thenSizeOfReturnedCollectionEqualsToTwo() {
        bookingRepository.clear();
        Ticket ticket1 = new Ticket();
        Ticket ticket2 = new Ticket();
        ticket1.setUserEmail("Flade@gmail.com");
        ticket2.setUserEmail("Sky@gmail.com");
        bookingRepository.save(ticket1.getUserEmail(), ticket1);
        bookingRepository.save(ticket2.getUserEmail(), ticket2);
        assertEquals(2, bookingRepository.getAll().size());
    }

    @Test
    void remove_whenTicketIsRemoved_thenCollectionDoesNotContainThisUser() {
        Ticket ticket1 = new Ticket();

        ticket1.setUserEmail("Sky@gmail.com");
        bookingRepository.save(ticket1.getUserEmail(), ticket1);
        bookingRepository.remove("Sky@gmail.com");
        assertEquals(Collections.emptySet(), bookingRepository.getTicketsByUserEmail("Sky@gmail.com"));
    }
}
