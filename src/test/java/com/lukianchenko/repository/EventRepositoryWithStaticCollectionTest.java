package com.lukianchenko.repository;

import com.lukianchenko.domain.Event;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class EventRepositoryWithStaticCollectionTest {

    private EventRepository eventRepository = new EventRepositoryWithStaticCollection();

    @Test
    void saveAndGetByName_whenNewEventIsSaved_thenEventRepositoryContainsThisEvent() {
        Event event = new Event();
        event.setName("Blade");
        eventRepository.save(event.getName(), event);
        assertEquals(event, eventRepository.getByName("Blade"));
    }

    @Test
    void getAll_whenTwoElementsAreSaved_thenSizeOfReturnedCollectionEqualsToTwo() {
        eventRepository.clear();
        Event event1 = new Event();
        Event event2 = new Event();
        event1.setName("Slade");
        event2.setName("Bly");
        eventRepository.save(event1.getName(), event1);
        eventRepository.save(event2.getName(), event2);
        assertEquals(2, eventRepository.getAll().size());
    }

    @Test
    void remove_whenEventIsRemoved_thenCollectionDoesNotContainThisEvent() {
        Event event2 = new Event();
        event2.setName("Sky");
        eventRepository.save(event2.getName(), event2);
        eventRepository.remove("Sky");
        assertNull( eventRepository.getByName("Sky"));
    }

}
