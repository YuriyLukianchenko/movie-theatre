package com.lukianchenko.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuditoriumRepositoryWithStaticColectionTest {

    private AuditoriumRepositoryWithStaticCollection auditoriumRepository = new AuditoriumRepositoryWithStaticCollection();

    @BeforeEach
    void init() {
        auditoriumRepository.init();
    }

    @Test
    void getByName_whenInvokeMethodWithRedHallParameter_thenAuditoriumWithSameNameReturned() {
        String name = "RedHall";
        assertEquals(name, auditoriumRepository.getByName(name).getName());
    }

    @Test
    void getAll_whenInvokeMethod_thenReturnedCollectionSizeEqualsTo3() {
        assertEquals(3, auditoriumRepository.getAll().size());
    }
}
