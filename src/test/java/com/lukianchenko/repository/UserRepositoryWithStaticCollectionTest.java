package com.lukianchenko.repository;

import com.lukianchenko.domain.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserRepositoryWithStaticCollectionTest {

    private UserRepository userRepository = new UserRepositoryWithStaticCollection();
    
    @Test
    void saveAndGetByName_whenNewUserIsSaved_thenUserRepositoryContainsThisUser() {
        User user = new User();
        user.setEmail("Blade@gmail.com");
        userRepository.save(user.getEmail(), user);
        assertEquals(user, userRepository.getByEmail("Blade@gmail.com"));
    }

    @Test
    void getAll_whenTwoElementsAreSaved_thenSizeOfreturnedCollectionEqualsToTwo() {
        User user1 = new User();
        User user2 = new User();
        user1.setEmail("Blade@gmail.com");
        user2.setEmail("Sky@gmail.com");
        userRepository.save(user1.getEmail(), user1);
        userRepository.save(user2.getEmail(), user2);
        assertEquals(2, userRepository.getAll().size());
    }

    @Test
    void remove_whenUserIsRemoved_thenCollectionDoesNotContainThisUser() {
        User user2 = new User();
        user2.setEmail("Sky@gmail.com");
        userRepository.save(user2.getEmail(), user2);
        userRepository.remove("Sky@gmail.com");
        assertNull( userRepository.getByEmail("Sky@gmail.com"));
    }
}
